const tailwind = require('../tailwind')

module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "/portfolio"

  siteTitle: 'Manuel Thomsen - Digital Design', // Navigation and Site Title
  siteTitleAlt: 'Skelegon - Web Design', // Alternative Site title for SEO
  siteTitleShort: 'Skelegon', // short_name for manifest
  siteHeadline: 'Modern, beautiful and fast digital presence', // Headline for schema.org JSONLD
  siteUrl: 'www.skelegon.dk', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteLogo: '/logo.png', // Used for SEO and manifest
  siteDescription: 'Modern & Simple Websites',
  author: 'Manuel Thomsen', // Author for schema.org JSONLD
  //TODO:change social links
  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: '@skelegonDK', // Twitter Username
  ogSiteName: 'skelegonmedia', // Facebook Site Name
  // ogLanguage: 'en_US', // Facebook Language

  // Manifest and Progress color
  themeColor: '#829d93',
  backgroundColor: tailwind.colors.black,
}
