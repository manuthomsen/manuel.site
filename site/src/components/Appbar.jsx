//Components
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Link} from 'gatsby';


const styles = {
  root: {
    flexGrow: 1,
  },
  button: {
    fontSize:"0.8rem",
    margin: '0.5rem',
    color: "#f7444e",
  },
  nav: {
    background:"transparent",
    boxShadow:"none",
  },
  typography: {
    fontSize:"1rem",
      color:"#f7444e",
      flexGrow:1
  },
  links: {
    display:'flex'
  }
};

function SimpleAppBar(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.nav}>
        <Toolbar>
          <Typography component={Link} to="/" variant="h6" color="inherit" className={classes.typography}>
            Home
          </Typography>
          <Button component={Link} to="/media" className={classes.button} >Video</Button>
          <Button component={Link} to="/games" className={classes.button} >Gamejams</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

SimpleAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleAppBar);