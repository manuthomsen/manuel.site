import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { Parallax } from 'react-spring/addons.cjs'

// Components
import Layout from '../components/Layout'
import ProjectCard from '../components/ProjectCard'
import SkillCard from '../components/SkillCard'


// Elements
import Inner from '../elements/Inner'
import { Title, BigTitle, Subtitle,FooterTitle } from '../elements/Titles'

// Views
import Hero from '../views/Hero'
import Projects from '../views/Projects'
import About from '../views/About'
import Contact from '../views/Contact'
import Gallery from '../views/Gallery'
import Skills from '../views/Skills'
import Services from '../views/Services'
// Images
import avatar from '../images/avatar.webp'
import andrea from '../images/andrea.webp'
import sune from '../images/Sune.webp'
import sassLogo from '../images/sass.svg'
import gatsbyLogo from '../images/gatsby-logo.svg'
import swiftLogo from '../images/Swift_logo.svg'
import wordpressLogo from '../images/Wordpress-Logo.svg'
import PsLogo from '../images/Photoshop_logo.svg'
import AiLogo from '../images/Illustrator_logo.svg'
import solidify from '../images/solidify.webp'
// Documents
import ManuelCVDK from '../documents/manueldkcv.pdf'


const ProjectsWrapper = styled.div`
  ${tw`flex flex-wrap justify-between mt-8`};
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: repeat(2, 1fr);
  @media (max-width: 1200px) {
    grid-gap: 3rem;
  }
  @media (max-width: 900px) {
    grid-template-columns: 1fr;
    grid-gap: 2rem;
  }
`
const SkillWrapper = styled.div`
${tw`flex flex-wrap justify-between mt-8`};
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 1fr 1fr;
  @media (max-width: 1200px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 3rem;
  }
  @media (max-width: 900px) {
    grid-template-columns: 1fr;
    grid-gap: 2rem;
  }
`

const AboutHero = styled.div`
  ${tw`flex flex-col lg:flex-row items-center mt-8`};
`

const Avatar = styled.img`
  ${tw`rounded-full w-32 xl:w-48 shadow-lg h-auto`};
`

const AboutSub = styled.span`
  ${tw`text-white pt-12 lg:pt-0 lg:pl-12 text-2xl lg:text-3xl xl:text-4xl`};
`

const AboutDesc = styled.p`
  ${tw`text-grey-light text-lg md:text-xl lg:text-2xl font-sans pt-6 md:pt-12 text-justify`};
`

const ContactText = styled.p`
  ${tw`text-grey-light font-sans text-xl md:text-2xl lg:text-3xl`};
`

const Footer = styled.footer`
  ${tw`text-center text-grey absolute pin-b p-6 font-sans text-md lg:text-lg`};
`

const Index = () => (
  <>
    <Layout />
    <Parallax pages={10}>
      <Hero offset={0}>
        <BigTitle>
          Hello, <br /> I'm Manuel
        </BigTitle>
        <Subtitle>
          I'm a web developer
          </Subtitle>
          <Subtitle>
          I create modern and functional digital experiences
          </Subtitle>
      </Hero>
      <Projects offset={1}>
        <Title>Project Showcase</Title>
        <ProjectsWrapper>
          <ProjectCard
            title="Atlas Biograferne"
            link="https://www.biografjob.dk/"
            bg="linear-gradient(to right, #5F68C0 0%, #9B9B9B 100%)"
          >
            Assisted frontend development on landing page for Crafthouse.dk
          </ProjectCard>
          <ProjectCard
            title="Prolog Coffee"
            link="https://www.prologcoffee.com/"
            bg="linear-gradient(to right, #2860AF 0%, #44CEEF 100%)"
          >
            Assisted frontend development on landing page for Crafthouse.dk
          </ProjectCard>
          <ProjectCard
            title="Møbel Copenhagen"
            link="http://www.mobel-copenhagen.com/"
            bg="linear-gradient(to right, #222222 0%, #4d4d4d 100%)"
          >
            Assisted development on Internship at B14
          </ProjectCard>
          <ProjectCard
          title="Solidify"
          link="https://www.solidify.dk/"
          bg="linear-gradient(to right,#1714ca 0%, #D3DAE0 100%)"
          >
          Logo, illustration and photography
          </ProjectCard>
          <ProjectCard
          title="Krop Ret Op"
          link="https://www.kropretop.dk/"
          bg="linear-gradient(to right,#85A7AD 0%, #94DCDB 100%)"
          >
            Logo design
          </ProjectCard>
        </ProjectsWrapper>
      </Projects>
      <About offset={3}>
        <Title>About me</Title>
        <AboutHero>
          <Avatar src={avatar} alt="Manuel" />
          <AboutSub>
            As a developer I am a very visual and functional designer. 
            I love a good moodboard and simple details whenever I create something for any client.
            My greatest strength is my ability to create modern simple designs 
            and also create amazing photos to go with them.
          </AboutSub>
        </AboutHero>
        <AboutDesc>
        I was raised in two different continents. Having a Danish and Chilean background i've been forward and back a few times.
        Growing up I was confronted with subjects such as "language barriers", "religion" and "cultural differences" at a very early age.
        I believe that made me a very compassionate but objective person. In my work I like to be creative and open, but always with a business focused mindset.
        </AboutDesc>
      </About>
      <Skills offset={4}>
        <Title>Primary skills</Title>
        <SkillWrapper>
        <SkillCard
            title="SASS"
            img={sassLogo}
          >
          CSS preprocessor
          </SkillCard>
          <SkillCard
            title="React"
            img={gatsbyLogo}
          >
          Javascript Framework
          </SkillCard>
          <SkillCard
            title="Wordpress"
            img={wordpressLogo}
          >
          CMS Development
          </SkillCard>
          <SkillCard
            title="iOS"
            img={swiftLogo}
          >
          Swift Development
          </SkillCard>
          <SkillCard
            title="Photoshop"
            img={PsLogo}
          >
          Photo editing
          </SkillCard>
          <SkillCard
            title="Illustrator"
            img={AiLogo}
          >
          Vector art and design
          </SkillCard>
        </SkillWrapper>
      </Skills>
      <Gallery offset={6}>
      <Title>Galleries</Title>
        <ProjectsWrapper>
        <ProjectCard
            title="Eyes open wide"
            link="https://500px.com/skelegon/galleries/eyes-open-wide"
            img={andrea}
          >
            BTS Shoot for Rumpistol
          </ProjectCard>
          <ProjectCard
            title="Soul Asylum"
            link="https://500px.com/skelegon/galleries/sonde-mar-soul-asylum-bts"
            img={sune}
            >
            BTS Shoot for Son Demar
          </ProjectCard>
          <ProjectCard
            title="Solidify.dk"
            link="https://500px.com/skelegon/galleries/solidify"
            img={solidify}
          >
            Business photos 
          </ProjectCard>
          <ProjectCard
            title="more..."
            link={'#'}
            bg="linear-gradient(to right,#f7444e 0%, transparent 100%)"
          >
            Coming Soon
          </ProjectCard>
        </ProjectsWrapper>
      </Gallery>
      <Services offset={7}>
      <Title>Services</Title>
        <ProjectsWrapper>
          <ProjectCard
          title="Business Blog"
          bg="linear-gradient(to right,transparent 0%,#78bcc4 100%)"
          link="mailto:manuel@skelegon.com?subject=Basic Wordpress"
          >
          Affordable & Simple solutions with Wordpress
          </ProjectCard>
          <ProjectCard
          title="Digital Business card"
          bg="linear-gradient(to right,transparent 0%,#78bcc4 100%)"
          link="mailto:manuel@skelegon.com?subject=Digital Business Card"
          >
          Stylish and modern one page websites
          </ProjectCard>
          <ProjectCard
          title="Photography"
          bg="linear-gradient(to right,transparent 0%,#78bcc4 100%)"
          link="mailto:manuel@skelegon.com?subject=Photography"
          
          >
          Visual storytelling for your company
          </ProjectCard>
          <ProjectCard
          title="Logo Design"
          bg="linear-gradient(to right,transparent 0%,#78bcc4 100%)"
          link="mailto:manuel@skelegon.com?subject=Logo Design"
          
          >
          Visual Identity
          </ProjectCard>
        </ProjectsWrapper>
      </Services>
      <Contact offset={9}>
        <Inner>
          <Title>Get in touch</Title>
          <ContactText>
            Say <a href="mailto:manuel@skelegon.com">Hi</a> or find me on{' '}
            <a href="https://www.instagram.com/skelegon/">Instagram</a> and <a href="https://www.linkedin.com/in/manuel-erik-sotomayor-thomsen/">Linkedin</a>
          </ContactText>
          <FooterTitle>Tell me about your idea!</FooterTitle>
        </Inner>
        <Footer>
          
        </Footer>
      </Contact>
    </Parallax>
  </>
)

export default Index
console.log(`
...................___...........|"|......
.....>X<........../\\#/\\........._|_|_......
....(o.o)......../(o.o)\\........(o.o).....
ooO--(_)--Ooo-ooO--(_)--Ooo-ooO--(_)--Ooo-
`);
console.log(`

..__.._.........._............................
./._\\|.|.__.___.|.|..___...__._...___..._.__..
.\\.\\.|.|/.//._.\\|.|./._.\\./._\\.|./._.\\.|.'_.\\.
._\\.\\|...<|..__/|.||..__/|.(_|.||.(_).||.|.|.|
.\\__/|_|\\_\\\\___||_|.\\___|.\\__,.|.\\___/.|_|.|_|
..........................|___/...............

`);