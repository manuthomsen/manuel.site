import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { Parallax } from 'react-spring/addons.cjs'

// Components
import Layout from '../components/Layout'
import ProjectCard from '../components/ProjectCard'

// Views
import Hero from '../views/Hero'

const ProjectsWrapper = styled.div`
  ${tw`flex flex-wrap justify-between mt-8`};
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: 1fr;
  @media (max-width: 1200px) {
    grid-gap: 3rem;
  }
  @media (max-width: 900px) {
    grid-template-columns: 1fr;
    grid-gap: 2rem;
  }
`

const NoPage = () => (
    <>
      <Layout />
      <Parallax pages={1}>
        <Hero offset={0}>
          <ProjectsWrapper>
            <ProjectCard
              title="404 - Wrong page"
              link="/"
              bg="#e2474b"
            >
            Click here to go back to the home page
             </ProjectCard>
            </ProjectsWrapper>
            </Hero>
        </Parallax>
  </>
)

export default NoPage