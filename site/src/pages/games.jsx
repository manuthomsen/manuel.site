import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { Parallax } from 'react-spring/addons.cjs'

// Components
import Layout from '../components/Layout'
import ProjectCard from '../components/ProjectCard'

// Elements
import Inner from '../elements/Inner'
import { Title, BigTitle, FooterTitle } from '../elements/Titles'

// Views
import Hero from '../views/Hero'
import Contact from '../views/Contact'
import Gallery from '../views/Gallery'
import About from '../views/About'
// Images
import Run from '../images/runLike Hell.gif'
import avatar from '../images/pixelplayer.gif'
import sky from '../images/sky.gif'
import skyP from '../images/sky.webp'
import RunP from '../images/runLike Hell.webp'
import bumperP from '../images/bumper.webp'
import portrait from '../images/portrait3.png'

const ProjectsWrapper = styled.div`
  ${tw`flex flex-wrap justify-between mt-8`};
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: repeat(2, 1fr);
  @media (max-width: 1200px) {
    grid-gap: 3rem;
  }
  @media (max-width: 900px) {
    grid-template-columns: 1fr;
    grid-gap: 2rem;
  }
`

const AboutHero = styled.div`
  ${tw`flex flex-col lg:flex-row items-center mt-8`};
`

const Avatar = styled.img`
  ${tw`rounded-full w-32 xl:w-48 shadow-lg h-auto`};
`

const AboutSub = styled.span`
  ${tw`text-white pt-12 lg:pt-0 lg:pl-12 text-2xl lg:text-3xl xl:text-4xl`};
`

const AboutDesc = styled.p`
  ${tw`text-grey-light text-lg md:text-xl lg:text-2xl font-sans pt-6 md:pt-12 text-justify`};
`

const ContactText = styled.p`
  ${tw`text-grey-light font-sans text-xl md:text-2xl lg:text-3xl`};
`

const Footer = styled.footer`
  ${tw`text-center text-grey absolute pin-b p-6 font-sans text-md lg:text-lg`};
`

const Index = () => (
  <>
    <Layout />
    <Parallax pages={5}>  
    <Hero offset={0}>
        <BigTitle>
          Game Jams
        </BigTitle> 
      </Hero>
      <About offset={1}>
        <Title>A Game of Love</Title>
        <AboutHero>
          <Avatar src={avatar} alt="Manuel" />
          <AboutSub>
          I've been passionate about video games for as long as I can remember, 
          but living in Chile as a child (80's & 90's) somehow I never wondered how games were made. 
          Even once I finally had a real computer, nobody knew what coding was in my village. 
          It was just "magic".
          </AboutSub>
        </AboutHero>
        <AboutDesc>
          Studying multimedia design I discovered game development through Javascript. I was very skeptical at first. 
          But then I discovered the magic of making actual ideas and game mechanics into code and how to interact with it. 
          This was to me a lifelong revelation, much like it was for me with photography a few years earlier.
          <br/><br/>
          One day I was asked to join a game jam with a friend, and from then on I was hooked. 
          Not only to being a part of making a tangible game, but also meeting, 
          working hard and discussing with other passionate individuals about design, sounds, 
          themes and anything that falls into interaction design.
        </AboutDesc>
      </About>
      <Gallery offset={2}>
      <Title>Game Jams</Title>
        <ProjectsWrapper>
        <ProjectCard
            title="Run Like Hell"
            link="https://itch.io/jam/ngj17/rate/136316"
            img={Run}
            webp={RunP}
          >
            Nordic Game Jam 2017
          </ProjectCard>
          <ProjectCard
            title="Bumper Blocks"
            link="https://globalgamejam.org/2017/games/bumper-blocks"
            webp={bumperP}
          >
            Global Game Jam 2017
          </ProjectCard>
          <ProjectCard
            title="Squid Warrios"
            link="https://thepinkzebra91.itch.io/squid-fighter"
          >
            Nordic Game Jam 2016
          </ProjectCard>
          
          <ProjectCard
            title="Melory"
            link="https://www.skelegon.dk/melory"
            img={sky}
            webp={skyP}            
          >
            Sound Memory Game
          </ProjectCard>
          <ProjectCard
            title="More soon"
            link="#"
          >
            Work in progress
          </ProjectCard>
        </ProjectsWrapper>
      </Gallery>
      
      <Contact offset={4}>
        <Inner>
          <Title>Get in touch</Title>
          <ContactText>
            Say <a href="mailto:manuel@skelegon.com">Hi</a> or find me on{' '}
            <a href="https://www.instagram.com/skelegon/">Instagram</a> and <a href="https://www.linkedin.com/in/manuel-erik-sotomayor-thomsen/">Linkedin</a>
          </ContactText>
          <FooterTitle>Tell me about your idea!</FooterTitle>
        </Inner>
        <Footer>
          
        </Footer>
      </Contact>
      </Parallax>
  </>
)

export default Index
console.log(`
...................___...........|"|......
.....>X<........../\\#/\\........._|_|_......
....(o.o)......../(o.o)\\........(o.o).....
ooO--(_)--Ooo-ooO--(_)--Ooo-ooO--(_)--Ooo-
`);
console.log(`

..__.._.........._............................
./._\\|.|.__.___.|.|..___...__._...___..._.__..
.\\.\\.|.|/.//._.\\|.|./._.\\./._\\.|./._.\\.|.'_.\\.
._\\.\\|...<|..__/|.||..__/|.(_|.||.(_).||.|.|.|
.\\__/|_|\\_\\\\___||_|.\\___|.\\__,.|.\\___/.|_|.|_|
..........................|___/...............

`);