import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { Parallax } from 'react-spring/addons.cjs'

// Components
import Layout from '../components/Layout'
import ProjectCard from '../components/ProjectCard'

// Elements
import Inner from '../elements/Inner'
import { Title, BigTitle,FooterTitle } from '../elements/Titles'

// Views
import Hero from '../views/Hero'
import Contact from '../views/Contact'
import Gallery from '../views/Gallery'
import About from '../views/About'

// Images
import avatarP from '../images/sky.webp'
import avatar from '../images/sky.gif'
import GymnastikP from '../images/Gymnastik.webp'
import FoktP from '../images/Fokt.webp'
import JazzP from '../images/Jazz.webp'
import Gymnastik from '../images/Gymnastik.gif'
import Fokt from '../images/Fokt.gif'
import Jazz from '../images/Jazz.gif'




const ProjectsWrapper = styled.div`
  ${tw`flex flex-wrap justify-between mt-8`};
  display: grid;
  grid-gap: 4rem;
  grid-template-columns: repeat(2, 1fr);
  @media (max-width: 1200px) {
    grid-gap: 3rem;
  }
  @media (max-width: 900px) {
    grid-template-columns: 1fr;
    grid-gap: 2rem;
  }
`

const AboutHero = styled.div`
  ${tw`flex flex-col lg:flex-row items-center mt-8`};
`

const Avatar = styled.img`
  ${tw`rounded-full w-32 xl:w-48 shadow-lg h-auto`};
`

const AboutSub = styled.span`
  ${tw`text-white pt-12 lg:pt-0 lg:pl-12 text-2xl lg:text-3xl xl:text-4xl`};
`

const AboutDesc = styled.p`
  ${tw`text-grey-light text-lg md:text-xl lg:text-2xl font-sans pt-6 md:pt-12 text-justify`};
`

const ContactText = styled.p`
  ${tw`text-grey-light font-sans text-xl md:text-2xl lg:text-3xl`};
`

const Footer = styled.footer`
  ${tw`text-center text-grey absolute pin-b p-6 font-sans text-md lg:text-lg`};
`

const Index = () => (
  <>
    <Layout />
    <Parallax pages={5}>  
    <Hero offset={0}>
        <BigTitle>
         Video Work
        </BigTitle>
      </Hero>
      <About offset={1}>
        <Title>Visualizing Passion</Title>
        <AboutHero>
          <Avatar src={avatar} alt="sky" />
          <AboutSub>
          I've been a very visual person since infancy. One of my earliest memories is of me asking for a few coins for candy, 
          just to spend them on classic arcade machines.
          </AboutSub>
        </AboutHero>
        <AboutDesc>
        Later in my 20's I discovered while working in an institution, that there was a great need for better video and photographic material. 
        So I started digging and investing on lessons and material to improve my skills. 
        In time, my experience and my interest brought me through many different projects. 
        Anything from documenting a child's development, to working on small film sets and music videos.
        </AboutDesc>
      </About>
      <Gallery offset={2}>
      <Title>Video Showcase</Title>
        <ProjectsWrapper>
        <ProjectCard
            title="Farum Greve Gymnastik"
            link="https://www.youtube.com/watch?v=aHg95XteoMg"
            img={Gymnastik}
            webp={GymnastikP}
          >
            Promotional Video
          </ProjectCard>
          <ProjectCard
            title="Apocalypse Man"
            link="https://www.youtube.com/watch?v=EgHqqQQX34I"
            img={Fokt}
            webp={FoktP}
          >
            Music Video
          </ProjectCard>
          <ProjectCard
            title="Jazz på Christiania"
            link="https://www.youtube.com/watch?v=2CFDVJzgTsc"
            img={Jazz}
            webp={JazzP}
          >
            News
          </ProjectCard>
        </ProjectsWrapper>
      </Gallery>
      
      <Contact offset={4}>
        <Inner>
          <Title>Get in touch</Title>
          <ContactText>
            Say <a href="mailto:manuel@skelegon.com">Hi</a> or find me on{' '}
            <a href="https://www.instagram.com/skelegon/">Instagram</a> and <a href="https://www.linkedin.com/in/manuel-erik-sotomayor-thomsen/">Linkedin</a>
          </ContactText>
          <FooterTitle>Tell me about your idea!</FooterTitle>
        </Inner>
        <Footer>
          
        </Footer>
      </Contact>
      </Parallax>
  </>
)

export default Index
console.log(`
...................___...........|"|......
.....>X<........../\\#/\\........._|_|_......
....(o.o)......../(o.o)\\........(o.o).....
ooO--(_)--Ooo-ooO--(_)--Ooo-ooO--(_)--Ooo-
`);
console.log(`

..__.._.........._............................
./._\\|.|.__.___.|.|..___...__._...___..._.__..
.\\.\\.|.|/.//._.\\|.|./._.\\./._\\.|./._.\\.|.'_.\\.
._\\.\\|...<|..__/|.||..__/|.(_|.||.(_).||.|.|.|
.\\__/|_|\\_\\\\___||_|.\\___|.\\__,.|.\\___/.|_|.|_|
..........................|___/...............

`);