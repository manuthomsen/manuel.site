import styled from 'styled-components'
import tw from 'tailwind.macro'
import { rotateAnimation } from '../styles/animations'
import triangle from '../images/triangle.svg'

export const Title = styled.h1`
  ${tw`text-2xl lg:text-4xl font-serif mb-8 tracking-wide relative inline-block`};
  text-shadow: 0 2px 10px rgba(247, 68, 78,0.2);
  color:#f7444e;
  &:before {
    content: '';
    width: 40px;
    height: 40px;
    background: url(${triangle});
    position: absolute;
    background-size: 40px;
    ${rotateAnimation('5s')};
    left: -60px;
    top: 5px;
  }
`

export const BigTitle = styled.h1`
  ${tw`text-5xl lg:text-6xl font-serif mb-6 tracking-wide`};
  text-shadow: 0 5px 35px rgba(247, 68, 78,0.2);
  color:#f7444e;
`

export const Subtitle = styled.p`
  ${tw`text-xl lg:text-4xl font-sans mt-8 xxl:w-3/4`};
  text-shadow: 0 2px 15px rgba(247, 68, 78,0.2);
  color:#f7444e;
`
export const FooterTitle = styled.h2`
${tw`text-4xl lg:text-4xl font-serif text-white mb-8 tracking-wide relative inline-block`};
text-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
`
