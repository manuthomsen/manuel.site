import React from 'react'
import PropTypes from 'prop-types'
import { Divider } from '../elements/Dividers'
import Content from '../elements/Content'
import Inner from '../elements/Inner'
import { UpDown, UpDownWide } from '../styles/animations'
import { colors } from '../../tailwind'
import SVG from '../components/SVG'

const About = ({ children, offset }) => (
  <>
    <Divider bg="#002c3e" clipPath="polygon(0 16%, 100% 4%, 100% 82%, 0 94%)" speed={0.2} offset={offset} factor={1} />
    <Divider speed={0.1} offset={offset}>
      <UpDown>
        <SVG
          icon="pets"
          hiddenMobile
          width={6}
          fill={colors.white}
          left="50%"
          top="75%"
        />
        <SVG
          icon="music"
          width={8}
          fill={'#a7a8a3'}
          left="70%"
          top="20%"
        />
        <SVG
          icon="videogame"
          width={8}
          fill={'#a7a8a3'}
          left="25%"
          top="5%"
        />
        <SVG
          icon="school"
          hiddenMobile
          width={24}
          fill={colors.white}
          left="80%"
          top="80%"
        />
      </UpDown>
      <UpDownWide>
        <SVG
          icon="music"
          hiddenMobile
          width={16}
          fill={colors.white}
          left="5%"
          top="80%"
        />
        <SVG
          icon="burger"
          hiddenMobile
          width={12}
          fill={colors.white}
          left="95%"
          top="50%"
        />
        <SVG
          icon="school"
          hiddenMobile
          width={8}
          fill={colors.white}
          left="85%"
          top="15%"
        />
        <SVG
          icon="burger"
          hiddenMobile
          width={8}
          fill={'#a7a8a3'}
          left="45%"
          top="10%"
        />
      </UpDownWide>
      <SVG
        icon="school"
        hiddenMobile
        width={6}
        fill={colors.white}
        left="4%"
        top="20%"
      />
      <SVG
        icon="videogame"
        width={12}
        fill={'#a7a8a3'}
        left="70%"
        top="60%"
      />
      <SVG
        icon="music"
        hiddenMobile
        width={6}
        fill={colors.white}
        left="10%"
        top="10%"
      />
      <SVG
        icon="burger"
        width={12}
        fill={'#a7a8a3'}
        left="20%"
        top="30%"
      />
      <SVG
        icon="videogame"
        width={8}
        fill={'#a7a8a3'}
        left="80%"
        top="70%"
      />
    </Divider>
    <Content speed={0.4} offset={offset} factor={1}>
      <Inner>{children}</Inner>
    </Content>
  </>
)

export default About

About.propTypes = {
  children: PropTypes.node.isRequired,
  offset: PropTypes.number.isRequired,
}
