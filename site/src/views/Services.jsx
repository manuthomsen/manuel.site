import React from "react";
import PropTypes from "prop-types";
import { Divider, DividerMiddle } from "../elements/Dividers";
import Content from "../elements/Content";
import Inner from "../elements/Inner";

const Services = ({ children, offset }) => (
    <>
        <DividerMiddle
            bg="linear-gradient(to right, #2f3456 0%, #406d96 100%)"
            speed={-0.2}
            offset={`${offset}.1`}
            factor={2}
        />
        <Content speed={0.4} offset={`${offset}.2`} factor={3}>
            <Inner>{children}</Inner>
        </Content>
        <Divider speed={0.1} offset={offset} factor={3}>
            

        </Divider>
    </>
    
);

export default Services;

Services.propTypes = {
    children: PropTypes.node.isRequired,
    offset: PropTypes.number.isRequired,
};
