import React from "react";
import PropTypes from "prop-types";
import { Divider, DividerMiddle } from "../elements/Dividers";
import Content from "../elements/Content";
import Inner from "../elements/Inner";
import { UpDown, UpDownWide } from "../styles/animations";
import { colors } from "../../tailwind";
import SVG from "../components/SVG";

const Projects = ({ children, offset }) => (
  <>
    <DividerMiddle
      bg="linear-gradient(to right, #f7f8f3 0%, #ffffff 100%)"
      speed={-0.2}
      offset={`${offset}.0`}
      factor={2}
    />
    <Content speed={0.4} offset={`${offset}.2`} factor={2}>
      <Inner>{children}</Inner>
    </Content>
    <Divider speed={0.1} offset={offset} factor={2}>
      <UpDown>
        <SVG
          icon="cafe"
          hiddenMobile
          width={24}
          fill="#002c3e"
          left="85%"
          top="75%"
        />
        <SVG icon="cafe" hiddenMobile width={8} fill="#002c3e" left="70%" top="20%" />
        <SVG icon="cafe" hiddenMobile width={8} fill="#002c3e" left="25%" top="5%" />
        <SVG
          icon="solidify"
          hiddenMobile
          width={24}
          fill="#002c3e"
          left="17%"
          top="60%"
        />
      </UpDown>
      <UpDownWide>
        <SVG
          icon="cinema"
          hiddenMobile
          width={24}
          fill="#002c3e"
          left="20%"
          top="90%"
        />
        <SVG
          icon="cinema"
          width={12}
          hiddenMobile
          fill="#002c3e"
          left="90%"
          top="30%"
        />
        <SVG
          icon="cinema"
          width={16}
          
          fill="#f7444e"
          left="70%"
          top="90%"
        />
        <SVG
          icon="solidify"
          width={24}
          fill="#f7444e"
          left="18%"
          top="75%"
        />
        <SVG icon="cinema" hiddenMobile width={6} fill="#002c3e" left="75%" top="10%" />
        <SVG
          icon="solidify"
          hiddenMobile
          width={8}
          fill="#002c3e"
          left="45%"
          top="10%"
        />
      </UpDownWide>
      <SVG icon="sofa" hiddenMobile width={6} fill="#002c3e" left="4%" top="20%" />
      <SVG
        icon="solidify"
        hiddenMobile
        width={12}
        fill={colors.whtie}
        left="80%"
        top="60%"
      />
      <SVG
        icon="sofa"
        
        width={24}
        fill="#f7444e"
        left="10%"
        top="10%"
      />
      <SVG icon="sofa" hiddenMobile width={12} fill="#002c3e" left="29%" top="26%" />
      <SVG
        icon="solidify"
        
        width={16}
        fill="#f7444e"
        left="75%"
        top="30%"
      />
      <SVG icon="sofa" hiddenMobile width={8} fill="#002c3e" left="80%" top="70%" />
    </Divider>
  </>
);

export default Projects;

Projects.propTypes = {
  children: PropTypes.node.isRequired,
  offset: PropTypes.number.isRequired
};
