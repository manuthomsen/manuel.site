import React from "react";
import PropTypes from "prop-types";
import { Divider, DividerMiddle } from "../elements/Dividers";
import Content from "../elements/Content";
import Inner from "../elements/Inner";
import { UpDown, UpDownWide } from "../styles/animations";
import { colors } from "../../tailwind";
import SVG from "../components/SVG";

const Gallery = ({ children, offset }) => (
    <>
        <DividerMiddle
            bg="linear-gradient(to right, #2b4d59 0%, #39998e 100%)"
            speed={-0.2}
            offset={`${offset}.1`}
            factor={2}
        />
        
        <Divider speed={0.1} offset={offset}>
            <UpDown>
                <SVG
                    icon="camera"
                    hiddenMobile
                    width={24}
                    fill={'#a7a8a3'}
                    left="85%"
                    top="75%"
                />
                <SVG
                    icon="camera"
                    width={8}
                    fill={'#a7a8a3'}
                    left="70%"
                    top="20%"
                />
                <SVG
                    icon="camera"
                    width={8}
                    fill={'#a7a8a3'}
                    left="25%"
                    top="25%"
                />
                <SVG
                    icon="camera"
                    hiddenMobile
                    width={24}
                    fill={'#a7a8a3'}
                    left="17%"
                    top="45%"
                />
            </UpDown>
            <UpDownWide>
                <SVG
                    icon="camera"
                    hiddenMobile
                    width={24}
                    fill={'#a7a8a3'}
                    left="20%"
                    top="75%"
                />
                <SVG
                    icon="camera"
                    width={12}
                    fill={'#a7a8a3'}
                    left="90%"
                    top="30%"
                />
                <SVG
                    icon="camera"
                    width={16}
                    fill={'#a7a8a3'}
                    left="70%"
                    top="75%"
                />
                <SVG
                    icon="camera"
                    hiddenMobile
                    width={16}
                    fill={'#a7a8a3'}
                    left="18%"
                    top="65%"
                />
                <SVG
                    icon="camera"
                    width={6}
                    fill={'#a7a8a3'}
                    left="75%"
                    top="20%"
                />
                <SVG
                    icon="camera"
                    width={16}
                    fill={'#a7a8a3'}
                    left="45%"
                    top="20%"
                />
            </UpDownWide>
        </Divider>
        <Content speed={0.4} offset={`${offset}.2`} factor={2}>
            <Inner>{children}</Inner>
        </Content>
    </>
);

export default Gallery;

Gallery.propTypes = {
    children: PropTypes.node.isRequired,
    offset: PropTypes.number.isRequired,
};
