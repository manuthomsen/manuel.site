import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { colors } from '../../tailwind'
import { Divider } from '../elements/Dividers'
import Content from '../elements/Content'
import { UpDown, UpDownWide } from '../styles/animations'
import SVG from '../components/SVG'
import portrait from '../images/portrait2.png'


const Wrapper = styled.div`
  ${tw`w-full xl:w-2/3`};
`
const Hero = ({ children, offset }) => (
  <>
  <Content speed={0.4} offset={offset}>
      <Wrapper>{children}</Wrapper>
    </Content>

    <Divider speed={0.2} offset={offset} bg={`linear-gradient(to top, #002c3e 5%, transparent 100%),url(${portrait})`}>
      <UpDown>
        <SVG icon="devices" hiddenMobile width={48} fill={'#f7f8f3'} left="60%" top="70%" />
        <SVG icon="accessibility" width={6} fill={'#a7a8a3'} left="90%" top="15%" />
        <SVG icon="mail" width={8} fill={'#8788a3'} left="95%" top="90%" />
        <SVG icon="devices" hiddenMobile width={24} fill={'#a7a8a3'} left="40%" top="80%" />
        <SVG icon="explore" width={8} fill={'#a7a8a3'} left="25%" top="5%" />
        <SVG icon="mail" width={6} fill={'#8788a3'} left="10%" top="10%" />
        <SVG icon="accessibility" width={12} fill={'#8788a3'} left="80%" top="30%" />
        <SVG icon="thumbsup" width={16} fill={'#a7a8a3'} left="10%" top="50%" />
        <SVG icon="question" width={8} fill={'#a7a8a3'} left="80%" top="70%" />
      </UpDown>
      <UpDownWide>
        <SVG icon="devices" hiddenMobile width={16} fill={'#a7a8a3'} left="80%" top="10%" />
        <SVG icon="explore" width={12} fill={'#f7f8f3'} left="90%" top="50%" />
        <SVG icon="question" width={16} fill={'#a7a8a3'} left="70%" top="90%" />
        <SVG icon="explore" width={16} fill={'#8788a3'} left="30%" top="65%" />
        <SVG icon="question" width={6} fill={'#8788a3'} left="75%" top="10%" />
        <SVG icon="mail" hiddenMobile width={8} fill={'#8788a3'} left="45%" top="10%" />
        <SVG icon="thumbsup" hiddenMobile width={24} fill={'#a7a8a3'} left="5%" top="70%" />
        <SVG icon="question" width={6} fill={'#8788a3'} left="4%" top="20%" />
        <SVG icon="thumbsup" width={12} fill="#8788a3" left="50%" top="60%" />
      </UpDownWide>
    </Divider>
    
  </>
)

export default Hero

Hero.propTypes = {
  children: PropTypes.node.isRequired,
  offset: PropTypes.number.isRequired,
}
