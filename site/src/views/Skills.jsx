import React from "react";
import PropTypes from "prop-types";
import { Divider, DividerMiddle } from "../elements/Dividers";
import Content from "../elements/Content";
import Inner from "../elements/Inner";
import { UpDown, UpDownWide } from "../styles/animations";
import { colors } from "../../tailwind";
import SVG from "../components/SVG";

const Skills = ({ children, offset }) => (
    <>
        <Divider
            bg="#23262b"
            clipPath="polygon(0 16%, 100% 4%, 100% 82%, 0 94%)"
            speed={0.2}
            offset={`${offset}.1`}
            factor={3}
        />
        <Divider speed={0.1} offset={offset}>
            <UpDown>
                <SVG
                    icon="wordpress"
                    hiddenMobile
                    width={6}
                    fill={'#8788a3'}
                    left="50%"
                    top="75%"
                />
                <SVG
                    icon="swift"
                    hiddenMobile
                    width={8}
                    fill={'#a7a8a3'}
                    left="70%"
                    top="20%"
                />
                <SVG
                    icon="react"
                    width={8}
                    fill={'#a7a8a3'}
                    left="25%"
                    top="25%"
                />
                <SVG
                    icon="wordpress"
                    hiddenMobile
                    width={24}
                    fill={'#8788a3'}
                    left="65%"
                    top="80%"
                />
            </UpDown>
            <UpDownWide>
                <SVG
                    icon="sass"
                    hiddenMobile
                    width={16}
                    fill={'#8788a3'}
                    left="5%"
                    top="80%"
                />
                <SVG
                    icon="wordpress"
                    hiddenMobile
                    width={12}
                    fill={'#8788a3'}
                    left="95%"
                    top="50%"
                />
                <SVG
                    icon="react"
                    hiddenMobile

                    width={8}
                    fill={'#8788a3'}
                    left="85%"
                    top="25%"
                />
                <SVG
                    icon="react"
                    hiddenMobile
                    width={8}
                    fill={'#a7a8a3'}
                    left="45%"
                    top="30%"
                />
            </UpDownWide>
            <SVG
                icon="react"
                hiddenMobile

                width={6}
                fill={'#8788a3'}
                left="4%"
                top="25%"
            />
            <SVG
                icon="wordpress"
                width={12}
                fill={'#a7a8a3'}
                left="70%"
                top="60%"
            />
            <SVG
                icon="react"
                hiddenMobile

                width={6}
                fill={'#8788a3'}
                left="10%"
                top="10%"
            />
            <SVG
                icon="sass"
                width={12}
                fill={'#a7a8a3'}
                left="20%"
                top="30%"
            />
            <SVG
                icon="swift"
                width={8}
                fill={'#a7a8a3'}
                left="80%"
                top="70%"
            />
        </Divider>
        <Content speed={0.4} offset={offset} factor={3}>
            <Inner>{children}</Inner>
        </Content>
    </>
);

export default Skills;

Skills.propTypes = {
    children: PropTypes.node.isRequired,
    offset: PropTypes.number.isRequired,
};
